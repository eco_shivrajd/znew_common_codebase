<?php 
/******************************************************
 * File Name	: db.class.php
 *******************************************************/	
	include("includes/config.php");

	#FUNCTION TO CLEAR INPUT VARIABLES 
	function cleanVar($input)  {
	   //$input = mysql_real_escape_string($input);
		if(!is_array($input))
		{
		$input = htmlspecialchars($input, ENT_IGNORE, 'utf-8');
	   	$input = strip_tags($input);
	   	$input = stripslashes($input);
	   	$input = preg_replace("/<script.*?\/script>/s", "", $input);	
		}
	   return $input;
	}
	function removeSpecial($string){
		$newString = preg_replace("/[^a-zA-Z]/", "", $string);
		return $newString;
	}
#class DB extends PDO Class
class DB 
{	
	public function add_order($client_id,$order_no, $order_date, $order_by_id, $lat, $long, $shop_id, $shop_order_status,$super_stockist_id,$dist_id) {
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
        echo '';print_r($conn);
		echo $order_sql = "INSERT INTO `tbl_orders` 
		(`order_no`, `ordered_by`, `order_date`, `shop_id`, `superstockistid`,`distributorid`,
		`total_items`, `total_cost`, `lat`, `long`, `shop_order_status`)
		VALUES ('$order_no', '$order_by_id', '$order_date', '$shop_id', '$super_stockist_id', '$dist_id', 
		'$total_items', '$total_cost', '$lat', '$long', '$shop_order_status')"; //o.shop_order_status = ".$order_status."
        
		$result = mysqli_query($conn, $order_sql);   
		$return_id = mysqli_insert_id($conn);
		echo $return_id;die();
		mysqli_close($conn);
		return $return_id;
	}
	public function add_order_details($order_product_details) {
		$client_id=$order_product_details['client_id'];
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
		$fields = "";
		$values = "";
		if(isset($order_product_details['producthsn']) && $order_product_details['producthsn'] != '')
		{			
			$fields.= ", producthsn";
			$values.= ", '".$order_product_details['producthsn']."'";			
		}
		if(isset($order_product_details['product_variant_weight2']) && $order_product_details['product_variant_weight2'] != '')
		{			
			$fields.= ", product_variant_weight2";
			$values.= ", '".$order_product_details['product_variant_weight2']."'";			
		}
		if(isset($order_product_details['product_variant_unit2']) && $order_product_details['product_variant_unit2'] != '')
		{			
			$fields.= ", product_variant_unit2";
			$values.= ", '".$order_product_details['product_variant_unit2']."'";			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0)
		{			
			$fields.= ", campaign_applied, campaign_type, campaign_sale_type";
			$values.= ", '".$order_product_details['campaign_applied']."', '".$order_product_details['campaign_type']."', '".$order_product_details['campaign_sale_type']."'";			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0 && $order_product_details['campaign_type'] == 'free_product')
		{			
			$fields.= ", free_product_details";
			$values.= ", '".$order_product_details['free_product_details']."'";			
		}
		
		echo $order_sql = "INSERT INTO `tbl_order_details` 
		( `order_id`, `brand_id`, `cat_id`, `product_id`, `product_variant_id`, 
		`product_quantity`, `product_variant_weight1`, `product_variant_unit1`, 
		`product_unit_cost`, `product_total_cost`, `product_cgst`, 
		`product_sgst`, `p_cost_cgst_sgst`, `order_status`
		$fields) 
		VALUES ('".$order_product_details['order_record_id']."', '".$order_product_details['brand_id']."',
		'".$order_product_details['cat_id']."', '".$order_product_details['product_id']."' ,
		'".$order_product_details['product_variant_id']."', 
		'".$order_product_details['product_quantity']."', '".$order_product_details['product_variant_weight1']."',
		'".$order_product_details['product_variant_unit1']."', 
		'".$order_product_details['product_unit_cost']."', '".$order_product_details['product_total_cost']."',
		'".$order_product_details['product_cgst']."', 
		'".$order_product_details['product_sgst']."', '".$order_product_details['p_cost_cgst_sgst']."',
		'".$order_product_details['order_status']."' $values)";
		$result = mysqli_query($conn, $order_sql);   
		$return_id = mysqli_insert_id($conn);
		mysqli_close($conn);
		return $return_id;
	}
	public function add_varient($client_id,$orderappid, $variantweight, $weightquantity, $unit, $variantsize, $variantunit, $shopid, $shopnme, $orderid, $totalcost,$product_varient_id, $productid, $productnm, $campaign_applied=null, $campaign_type=null, $campaign_sale_type=null) {
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
        
		
		$fields = "";
		$values = "";
		
		if($product_varient_id=="") {
			$product_varient_id=0;
		}
		if($campaign_applied == 'yes')
		{			
			$campaign_applied = '0';
			$fields.= ", campaign_type, campaign_sale_type";
			$values.= ", '".$campaign_type."', '".$campaign_sale_type."'";
		}else{
			$campaign_applied = '1';
		}
		
		$order_sql = "INSERT INTO `tbl_variant_order` 
			( `orderappid`, `variantweight`, weightquantity, unit, `variantsize`, `variantunit`, `shopid`, `shopnme`, `orderid`, `totalcost`, `productid`, product_varient_id, `productnm`,`campaign_applied` $fields) 
			VALUES ('".$orderappid."', '".$variantweight."', '".$weightquantity."', '".$unit."' ,
			'".$variantsize."', '".$variantunit."', '".$shopid."', '".$shopnme."', '".$orderid."', '".$totalcost."',
			'".$productid."', '".$product_varient_id."', '".$productnm."', '".$campaign_applied."' $values)";
		
		
		$result = mysqli_query($conn, $order_sql);   
		$return_id = mysqli_insert_id($conn);
		mysqli_close($conn);
		return $return_id;
	}
	
	function add_order_c_p_discount($client_id,$campaign_id,$order_variant_id,$discount_amount,$discount_percent,$actual_amount)
	{
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
        
		echo $sthsstockistsql = "INSERT INTO `tbl_order_cp_discount` 
			( `campaign_id`, `order_variant_id`,  `discount_amount` ,  `discount_percent` , `actual_amount`) 
			VALUES ('".$campaign_id."', '".$order_variant_id."', '".$discount_amount."', '".$discount_percent."' ,
			'".$actual_amount."')";
		$result = mysqli_query($conn, $sthsstockistsql); 
		$return_id = mysqli_insert_id($conn);
		mysqli_close($conn);
		return $return_id;
	}
	public function update_order($client_id,$order_id, $total_items , $total_cost, $total_order_gst_cost,$offer_provided) {
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
        $sthsstockistsql = "UPDATE `tbl_orders` SET `total_items` = '".$total_items."' , 
		`total_cost` = '".$total_cost."', `total_order_gst_cost`= '".$total_order_gst_cost."',
		`offer_provided` = '".offer_provided."' WHERE `id` = '".$order_id."'";
		$result = mysqli_query($conn, $sthsstockistsql); 
		$return_id = mysqli_insert_id($conn);
		mysqli_close($conn);
		return $return_id;
	}
	function add_order_c_n_f_product($client_id,$campaign_id,$c_p_order_variant_id,$f_p_order_variant_id)
	{
		$conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]) or die("cannot connect");
        $sthsstockistsql = "INSERT INTO `tbl_order_c_n_f_product` 
			( `campaign_id`, `c_p_order_variant_id`,  `f_p_order_variant_id`) 
			VALUES ('".$campaign_id."', '".$c_p_order_variant_id."', '".$f_p_order_variant_id."')";
		$result = mysqli_query($conn, $sthsstockistsql); 
		$return_id = mysqli_insert_id($conn);
		mysqli_close($conn);
		return $return_id;
	}
	
}
?>