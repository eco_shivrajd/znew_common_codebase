<?php
ini_set('display_errors', 1);
include 'db.php';
include 'wsJSON.php';
$json_string = $_POST['orderarray'];
$array = json_decode($json_string);

$userid = $array->userid;
$current_lat = $array->current_lat;
$current_long = $array->current_long;

$count=0;$total_order_cost=0;$total_order_gst_cost=0;$sale_count=0;$free_count=0;$total_count=0;

foreach ($array->company_list as $key => $company) {//order initial details shop level
	$client_id = $company->company_id;
	$distributor_id=$company->dist_id;
	$con = new Connection($DB_NAMES_ARRAY[$client_id]);	
	$JSONVar = new wsJSON($con);
    
    foreach ($company->order_list as $key1 => $order) {
        $order_id = $order->order_id;
        foreach ($order->product_details as $key2 => $product) {
            $product_id = $product->product_id;
            $product_varient_id = $product->product_varient_id;
            $product_quantity = $product->quantity;
            $product_price = $product->product_price;
            $total_cost=$product_quantity*$product_price;
            if($product->cgst != 0){ $cgst_value = (($total_cost * $product->cgst)/100); }
            if($product->sgst != 0){ $sgst_value = (($total_cost * $product->sgst)/100); }
            $total_gst = $total_cost;
            if($cgst_value != 0) { $total_gst = $total_gst + $cgst_value; }                
            if($sgst_value != 0) {  $total_gst = $total_gst + $sgst_value; }               
            $total_cost_with_tax = $total_gst;
            $order_status='1';
             $inserted = $JSONVar->fnplaceorder_customer($userid,$distributor_id,$current_lat, $current_long, $order_id,
                    $product_id, $product_varient_id,$product_price,$product_quantity,$total_cost
            ,$total_cost_with_tax,$order_status);
             
            
            $total_order_cost = $total_order_cost+$total_cost;
            $total_order_gst_cost = $total_order_gst_cost+$total_cost_with_tax;
            $sale_count = $sale_count+$product_quantity;
            $free_count = $free_count+0;
            $total_count = $total_count + $product_quantity;
        }
    }
}
$jsonOutput = array();
$jsonOutput['status']['responsecode'] = '0';
$jsonOutput['status']['entity'] = '1';
$jsonOutput['data']['invitation']['msg'] = "Order Placed Successfully.";
$jsonOutput['data']['total_cost'] = $total_order_cost;
$jsonOutput['data']['total_gst_cost'] = $total_order_gst_cost;
$jsonOutput['data']['total_quantity'] = $sale_count;
$jsonOutput['data']['sale_quantity'] = $free_count;
$jsonOutput['data']['free_quantity'] = $total_count;
echo json_encode($jsonOutput);