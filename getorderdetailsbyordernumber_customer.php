<?php
ini_set('display_errors', 1);
include 'db.php';
$orderid = $_POST['orderid'];
$client_id = $_POST['client_id'];



$jsonOutput = array();
$totalrowcount = 0;
$ohistory_array_temp1 = array();
if ($orderid != '' && $client_id != '') {
	$conn =	mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, $DB_NAMES_ARRAY[$client_id]);
     

    $where_clause_outer = "";
    $where_clause_outer = " AND order_id='$orderid' ";
    $order_sql = "SELECT tco.*,tu.firstname, pcatvar.category_name as cat_name,
           pcatvar.brand_name,pcatvar.product_name,pcatvar.product_variant1,pcatvar.product_variant2,
		   pcatvar.variant1_unit_name,pcatvar.variant2_unit_name
		   pcatvar.product_price
               FROM tbl_customer_orders tco  
                left join `tbl_user` tu  on tco.distributor_id=tu.id
                LEFT join pcatbrandvariant pcatvar on tco.product_id=pcatvar.product_id 
                 and tco.product_var_id=pcatvar.product_variant_id
                WHERE 1=1 $where_clause_outer  ";
    $result = mysqli_query($conn, $order_sql);
    $rowcount = mysqli_num_rows($result);
    if ($rowcount > 0) {
        $jsonOutput['status']['responsecode'] = '0';
        $jsonOutput['status']['entity'] = '1';
        while ($value = mysqli_fetch_array($result)) {
            $ohistory_array_temp['product_varient_id'] = $value['product_var_id'];
            $ohistory_array_temp['distributorid'] = $value['distributor_id'];
            $ohistory_array_temp['distributornm'] = $value['firstname'];
            $ohistory_array_temp['customernm'] = $value['customer_name'];
            $ohistory_array_temp['orderid'] = $value['id'];
            $ohistory_array_temp['ordernum'] = $value['order_id'];
            $ohistory_array_temp['variant_oder_id'] = '';
            //get product var details
			$var_name1="";$var_name2="";
			$weight = $value['product_variant1'];
			$weightunit = $value['variant1_unit_name']; 				
			if ($weight != '' ) {
				$var_name1 = $weight . "-" . $weightunit . " ";
			}				
			$size = $value['product_variant2'];
			$sizeunit = $value['variant2_unit_name']; 				
			if ($size != '' ) {
				$var_name2 = $size . "-" . $sizeunit . " ";
			}
				
            $ohistory_array_temp['productname'] = $value['product_name'] . " " . $var_name1;
            if (!empty($var_name2)) {
                $ohistory_array_temp['productname'] .= " " . $var_name2;
            }
            $ohistory_array_temp['order_date'] = $value['order_date'];
            $ohistory_array_temp['unitprice'] = $value['product_price'];
            $ohistory_array_temp['quantity'] = $value['product_quantity'];
            $ohistory_array_temp['totalcost'] = $value['total_cost_with_tax'];
            $ohistory_array_temp['catnm'] = $value['cat_name'];
            $ohistory_array_temp['brandnm'] = $value['brand_name'];
            $ohistory_array_temp['totalproductcount'] = '';
            $ohistory_array_temp1[] = $ohistory_array_temp;
        }
        if (!empty($ohistory_array_temp1))
            $jsonOutput['data']['orderdetails'] = $ohistory_array_temp1;
    }else {
        $jsonOutput['status']['responsecode'] = '1';
        $jsonOutput['status']['entity'] = '1';
        $jsonOutput['data']['ohistory'][] = "No records found";
    }
} else {
    $jsonOutput['status']['responsecode'] = '1';
    $jsonOutput['status']['entity'] = '1';
    $jsonOutput['data']['ohistory'][] = "No records found";
}
echo json_encode($jsonOutput);


