<?php 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/userManage.php";   
$userObj    =   new userManager($con,$conmain);

$user_details_id = $_POST['user_details_id'];
$user_type = $_POST['user_type'];
$user_details = $userObj->getAllUsersViewAllDetails($user_details_id);
//print"<pre>";print_r($user_details);
?>
<div class="modal-header">

<button type="button" name="btnPrint" id="btnPrint" class="btn btn-primary" >Take A print</button>


<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divOrderPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$user_type;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
			<tr>
				<td>Name</td>
				<td><?=$user_details['firstname'];?></td>				
			</tr>

            <?php
             if ($user_type =='Distributor' || $user_type =='DeliveryChannelPerson' || $user_type =='SalesPerson' || $user_type =='DeliveryPerson') 
             {
            ?>
            <tr>
				<td>Assign Super Stockist</td>
				<td><?=$user_details['sstockist_name'];?></td>				
			</tr>             
            <?php 	
             }
            ?>

            <?php
             if ($user_type =='SalesPerson' || $user_type =='DeliveryPerson') 
             {
            ?>
            <tr>
				<td>Assign Stockist</td>
				<td><?php
						$stockist_id = $user_details['stockist_id'];                        
						$count1 =explode(',', $stockist_id);
						$count11 = count($count1);                 
									for ($i=0; $i <$count11 ; $i++) 
									{ 
										$count2 = $count1[$i];
									   $sql1="SELECT id,firstname  FROM tbl_user where id ='".$count2."'";      
										$result1 = mysqli_query($con, $sql1);
											while ($row1 = mysqli_fetch_array($result1)) 
											{
											  echo $row1['firstname'].",";
											}  
									}                                                       
						?>
				</td>				
			</tr>             
            <?php 	
             }
            ?>


			<tr>
				<td>Email</td>
				<td><?=$user_details['email'];?></td>				
			</tr>
		    <tr>
				<td>Mobile No.</td>
				<td><?=$user_details['mobile'];?></td>				
			</tr>
			<tr>
				<td>Subarea</td>
				<td><?=$user_details['subarea_name'];?>
					
					<?php
						$subarea_ids = $user_details['subarea_ids'];                        
						$count1 =explode(',', $subarea_ids);
						$count11 = count($count1);                 
									for ($i=0; $i <$count11 ; $i++) 
									{ 
										$count2 = $count1[$i];
									   $sql1="SELECT subarea_id,subarea_name  FROM tbl_subarea where subarea_id ='".$count2."'";      
										$result1 = mysqli_query($con, $sql1);
											while ($row1 = mysqli_fetch_array($result1)) 
											{
											  echo $row1['subarea_name'].",";
											}  
									}                                                       
						?>
				</td>				
			</tr>
			<tr>
				<td>Taluka</td>
				<td>
					<?php
                           $suburb_ids = $user_details['suburb_ids'];                          
                           $count =explode(',', $suburb_ids);
                            $count1 = count($count);                 
									for ($i=0; $i <$count1 ; $i++) 
									{ 
											$count2 = $count[$i];
											$sql="SELECT id,suburbnm  FROM tbl_area where id ='".$count2."'";      
											$result = mysqli_query($con, $sql);
												while ($row = mysqli_fetch_array($result)) 
												{
												  echo $row['suburbnm'].",";
												}  
									}                                                       
						?>
				</td>				
			</tr>
			
			<tr>
				<td>City</td>
				<td><?=$user_details['cityname'];?></td>				
			</tr>
			<tr>
				<td>State</td>
				<td><?=$user_details['statename'];?></td>				
			</tr>
			<tr>
				<td>Office Phone No</td>
				<td><?=$user_details['office_phone_no'];?></td>				
			</tr>
			<tr>
				<td><b>Other Details</b></td>
				<td></td>				
			</tr>				
			
			<tr>
				<td>Account Name</td>
				<td><?=$user_details['accname'];?></td>				
			</tr>
			<tr>
				<td>Account No</td>
				<td><?=$user_details['accno'];?></td>				
			</tr>
		    <tr>
				<td>Branch</td>
				<td><?=$user_details['accbrnm'];?></td>				
			</tr>
			<tr>
				<td>Bank Name</td>
				<td><?=$user_details['bank_name'];?></td>				
			</tr>
			<tr>
				<td>IFSC Code</td>
				<td><?=$user_details['accifsc'];?></td>				
			</tr>
			<tr>
				<td>GST Number</td>
				<td><?=$user_details['gst_number_sss'];?></td>				
			</tr>

			</table>
</div>
</div>
</div>
</div>
</div>