<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";   
$userObj    =   new userManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{   
   //print"<pre>";print_r($_POST);
  // exit();
    $user_type=$_POST['user_type'];    
    /* Local user section */
    $common_user_id = $userObj->addCommonUserDetails($user_type);
    //$common_user_id = 14;
    if(isset($common_user_id) && $common_user_id!='')
    {
        $userid_local = $userObj->addAllLocalUserDetails($user_type,$common_user_id);          
    }
    else
    {
        echo '<script>alert("Something Wrong..!!");</script>';
    //  $userObj->addCommonUserCompanyDetails($userid_local);   
    }
    $email      =   fnEncodeString($_POST['email']);
    if($email != '')
        $userObj->sendUserCreationEmail();
    
    echo '<script>alert("User added successfully.");location.href="user_list.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageSupplyChain"; $activeMenu = "User List";
include "../includes/sidebar.php";
?>

<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">   
        <h3 class="page-title">User</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                 
                <li>
                    <i class="fa fa-home"></i>
                    <a href="accountant.php">User</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Add New User</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet box blue-steel">
                <div class="portlet-title"><div class="caption">Add New User</div></div>
                <div class="portlet-body">
                    <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                    <form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate=""  method="post" action="">          


                        <div class="form-group">
                            <label class="col-md-3">Select User Type:<span class="mandatory">*</span></label>
                            <div class="col-md-4">
                                <select name="user_type" id="user_type" 
                                class="form-control" required>
                                <option value="" selected>-- Select --</option>
                                <option value="Superstockist">Superstockist</option>
                                <option value="Distributor">Stockist</option>
                                <option value="SalesPerson">Sales Person</option>
								<option value="DeliveryPerson">DeliveryPerson</option>
                                <option value="Shopkeeper">Shopkeeper</option>
                                <option value="Accountant">Accountant</option>
                                <option value="DeliveryChannelPerson">Delivery Channel Person</option>
                                </select>
                            </div>
                        </div>                      

                       <div class="form-group" id="hidden_div1" style="display: none;">
                            <label class="col-md-3">Superstockist:<span class="mandatory">*</span></label>
                            <div class="col-md-4" id="div_select_superstockist">             
                            <select name="cmbSuperStockist" id="cmbSuperStockist"                 
                            class="form-control">
                            <option value="">-Select-</option>                                    
                            </select>                
                            </div>
                        </div>  

                        <div class="form-group" id="hidden_div2" style="display: none;">
                            <label class="col-md-3">Stockist:<span class="mandatory">*</span></label>
                            <div class="col-md-4" id="div_select_stockist">             
                            <select name="assign[]" multiple class="form-control">
                            <option value="">-Select-</option>                                    
                            </select>                
                            </div>
                        </div>                          

                       <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
                placeholder="Enter Name"
                data-parsley-trigger="change"               
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter name"
                data-parsley-maxlength="50"
                data-parsley-maxlength-message="Only 50 characters are allowed"             
                name="firstname" class="form-control">
              </div>
            </div>
             
            <div class="form-group">
              <label class="col-md-3">Username:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" id="username"
                placeholder="Enter Username"
                data-parsley-minlength="6"
                data-parsley-minlength-message="Username should be minimum 6 characters without blank spaces"          
                data-parsley-maxlength="50"
                data-parsley-maxlength-message="Only 50 characters are allowed"          
                data-parsley-type-message="Please enter Username, blank spaces are not allowed"        
                data-parsley-required-message="Please enter Username, blank spaces are not allowed"
                data-parsley-trigger="change"
                data-parsley-required="true"
                data-parsley-pattern="/^\S*$/" 
                data-parsley-error-message="Username should be 6-50 characters without blank spaces"
                name="username" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Password:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="password" id="password"
                placeholder="Enter Password"               
                data-parsley-minlength="6"
                data-parsley-minlength-message="Password should be minimum 6 characters without blank spaces"   
                data-parsley-maxlength="50"
                data-parsley-maxlength-message="Only 50 characters are allowed"   
                data-parsley-type-message="Password should be minimum 6 characters without blank spaces"           
                data-parsley-required-message="Please enter Password"
                data-parsley-trigger="change"
                data-parsley-required="true"
                data-parsley-pattern="/^\S*$/" 
                data-parsley-error-message="Password should be 6-50 characters without blank spaces"
                name="password" class="form-control placeholder-no-fix">
              </div>
            </div>
           <div class="form-group">
              <label class="col-md-3">Confirm Password:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="password" id="c_password"
                placeholder="Enter Confirm Password"
                data-parsley-trigger="change"   
                data-parsley-equalto="#password"
                data-parsley-equalto-message="Password does not match with confirm password"                
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter confirm password"       
                name="c_password" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Address:</label>
                <div class="col-md-4">
                    <textarea name="address"
                    placeholder="Enter Address"
                    data-parsley-trigger="change"
                    data-parsley-maxlength="200"
                    data-parsley-maxlength-message="Only 200 characters are allowed"                            
                    rows="4" class="form-control" ></textarea>
                </div>
            </div>
           
            <div id="working_area_top">
            <h4>Assign </h4>
            </div>
            <div class="form-group">
                <label class="col-md-3">State:</label>
                <div class="col-md-4">
                <select name="state"  id="state"
                class="form-control" onChange="fnShowCity(this.value)">
                <option selected disabled>-select-</option>
                <?php
                $sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
                $result = mysqli_query($con,$sql);
                while($row = mysqli_fetch_array($result))
                {
                    $cat_id=$row['id'];
                    $selected_state = '';
                    if($default_state == $row['id'])
                        $selected_state = 'selected';
                    echo "<option value='$cat_id' $selected_state>" . $row['name'] . "</option>";
                } ?>
                </select>
                </div>
            </div>          
            <div class="form-group" id="city_div" >
              <label class="col-md-3">District:</label>
              <div class="col-md-4" id="div_select_city">             
              <select name="city" id="city"                 
                class="form-control">
                <option selected value="">-Select-</option>                                     
                </select>                
              </div>
            </div>

            <div class="form-group" id="area_div">
              <label class="col-md-3">Taluka:</label>
              <div class="col-md-4" id="div_select_area">              
              <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                <option selected value="">-Select-</option>                                 
                </select>
             
              </div>
            </div>                     
            <div class="form-group" id="subarea_div" style="display:none;">
              <label class="col-md-3">Subarea:</label>
              <div class="col-md-4" id="div_select_subarea">
              <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
                <option selected value="">-Select-</option>                                 
                </select>
              </div>
            </div>
            <div id="working_area_bottom">              
            </div>
          
             <div class="form-group">
              <label class="col-md-3">Email:</label>

              <div class="col-md-4">
                <input type="text" id="email"
                placeholder="Enter E-mail"
                data-parsley-maxlength="100"
                data-parsley-maxlength-message="Only 100 characters are allowed"
                data-parsley-type="email"
                data-parsley-type-message="Please enter valid e-mail" 
                data-parsley-trigger="change"
                name="email" class="form-control"><span id="user-availability-status"></span>
              </div>
            </div>
            <div class="form-group">
                  <label class="col-md-3">Mobile Number:</label>

                  <div class="col-md-4">
                    <input type="text"  name="mobile"
                    placeholder="Enter Mobile Number"
                    data-parsley-trigger="change"                   
                    data-parsley-minlength="10"
                    data-parsley-maxlength="15"
                    data-parsley-maxlength-message="Only 15 characters are allowed"
                    data-parsley-pattern="^(?!\s)[0-9]*$"
                    data-parsley-pattern-message="Please enter numbers only"
                    class="form-control" value="">
                  </div>
                </div>
               
                <div class="form-group">
              <label class="col-md-3">GST Number:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text"
                placeholder="Enter GST Number"
                data-parsley-trigger="change"               
                data-parsley-required="#true" 
                data-parsley-required-message="Please enter GST Number"
                data-parsley-maxlength="15"
                data-parsley-maxlength-message="Only 15 characters are allowed" 
                data-parsley-minlength="15"
                data-parsley-minlength-message="Not Valid GST Number"   
                name="gstnumber" class="form-control">
              </div>
            </div>
             
                <input type="hidden" name="page_to_add" id="page_to_add" value="<?=$page_to_add;?>">

                        
                       
                        <div class="form-group">
                          <label class="col-md-3">Office Phone No.:</label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Office Phone No."
                            data-parsley-trigger="change"                   
                            data-parsley-minlength="10"
                            data-parsley-maxlength="15"
                            data-parsley-maxlength-message="Only 15 characters are allowed"
                            data-parsley-pattern="^(?!\s)[0-9]*$"
                            data-parsley-pattern-message="Please enter numbers only"
                            name="phone_no" 
                            class="form-control">
                          </div>
                        </div>   

                        <div class="form-group">
                        <label class="col-md-3">Latitude:</label>
                        <div class="col-md-4">
                        <input type="text" name="latitude"
                        placeholder="Enter Latitude"
                        class="form-control" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9!@#$%^&*+_=><,./:' ]*$">
                        </div>
                        </div>        
                        <div class="form-group">
                        <label class="col-md-3">Longitude:</label>
                        <div class="col-md-4">
                        <input type="text" name="longitude"
                        placeholder="Enter Longitude"
                        class="form-control" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9!@#$%^&*+_=><,./:' ]*$">
                        </div>
                        </div> 
                        
                         <div class="form-group">
                            <label class="col-md-3"><b>Bank Details</b></label>
                        </div>                      
                         <div class="form-group">
                            <label class="col-md-3">Account Name <span class="mandatory">*</span></label>
                            <div class="col-md-4">
                                <input type="text" 
                                placeholder="Account Name"
                                data-parsley-trigger="change"               
                                data-parsley-required="#true" 
                                data-parsley-required-message="Please enter accouont number"
                                data-parsley-maxlength="50"
                                data-parsley-maxlength-message="Only 50 characters are allowed"
                                name="accname" 
                                
                                class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Account Number"
                            data-parsley-trigger="change"               
                            data-parsley-required="#true" 
                            data-parsley-required-message="Please enter accouont number"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                     
                            name="accno" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Bank Name:<span class="mandatory">*</span></label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Bank Name"
                            data-parsley-trigger="change"               
                            data-parsley-required="#true" 
                            data-parsley-required-message="Please enter Bank Name"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="bank_name" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter Branch Name"
                            data-parsley-trigger="change"               
                            data-parsley-required="#true" 
                            data-parsley-required-message="Please enter branch Name"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="accbrnm" class="form-control" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
                          <div class="col-md-4">
                            <input type="text"
                            placeholder="Enter IFSC Code"
                            data-parsley-trigger="change"               
                            data-parsley-required="#true" 
                            data-parsley-required-message="Please enter ifsc Code"
                            data-parsley-maxlength="30"
                            data-parsley-maxlength-message="Only 30 characters are allowed"                         
                            name="accifsc" class="form-control">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-md-3">Status:</label>
                          <div class="col-md-4">
                            
                                <select name="user_status" id="user_status" class="form-control">
                                    <option value="Active" selected>Active</option>
                                    <option value="Inactive" >Inactive</option>
                                </select>
                            
                          </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                        <div class="col-md-4 col-md-offset-3">
                        <input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
                        <input type="hidden" name="hidAction" id="hidAction" value="add-users.php">
                         <button name="getlatlong" id="getlatlong" type="button"  class="btn btn-primary">Getlatlong</button>
                        <button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
                        <a href="user_list.php" class="btn btn-primary">Cancel</a>
                        </div>
                        </div><!-- /.form-group -->
                    </form>  
                </div>
            </div>
        </div>
    </div>
    </div>
</div>>
</div>
<?php include "../includes/footer.php";?>

<script type="text/javascript">
    $(function() { 

      $('#user_type').change(function()
      {
        if($('#user_type').val() == 'Distributor' || $('#user_type').val() == 'DeliveryChannelPerson')
        {                
                var user_type = 'Superstockist';
                var url = "getUserByTypeDropDown.php?user_type="+user_type+"&condition=noOnchange";
                CallAJAX(url,"div_select_superstockist");
                $('#hidden_div1').show(); 
                $('#hidden_div2').hide();
        }
        if ($('#user_type').val() == 'SalesPerson' || $('#user_type').val() == 'DeliveryPerson') 
        {
			var user_type = 'Superstockist';
			var url = "getUserByTypeDropDown.php?user_type="+user_type+"&condition=Onchange";
			CallAJAX(url,"div_select_superstockist");
			$('#hidden_div1').show(); 
        }
        if ($('#user_type').val() == 'Superstockist' || $('#user_type').val() == 'Accountant') 
        {               
			$('#hidden_div1').hide(); 
			$('#hidden_div2').hide();
        }
       
    });

});
</script>

<script>
function fnShowStockist(id)
{
    // alert('hi');
    //alert(id.value); 
                var user_type = 'Distributor';
                var url = "getUserByTypeDropDown.php?user_type="+user_type+"&user_id="+id.value;
                CallAJAX(url,"div_select_stockist");
                $('#hidden_div2').show(); 
}
</script>
<script>
function calculate_data_count(element_value){
    element_value = element_value.toString();
    var element_arr = element_value.split(','); 
    return element_arr.length;
}
function setSelectNoValue(div,select_element){
    var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
    document.getElementById(div).innerHTML  =   select_selement_section;
}
function fnShowCity(id_value) { 
    $("#city_div").show();  
    $("#area").html('<option value="">-Select-</option>');  
    $("#subarea").html('<option value="">-Select-</option>');   
    var page_to_add = $("#page_to_add").val();
    var param = '';
    if(page_to_add == 'superstockist')
        param = "&nofunction=nofunction";
    var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory"+param;
    CallAJAX(url,"div_select_city");    
}
function FnGetSuburbDropDown(id) {
    $("#area_div").show();  
    $("#subarea").html('<option value="">-Select-</option>');   
    var page_to_add = $("#page_to_add").val();
    var param = '';
    if(page_to_add == 'stockist')
        param = "&nofunction=nofunction";
    var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&multiple=multiple&function_name=FnGetSubareaDropDown"+param;
    CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) { 
    var suburb_str = $("#area").val();  
    var suburb_arr_count = calculate_data_count(suburb_str);
    if(suburb_arr_count == 1){//If single city selected then only show its related subarea  
        $("#subarea_div").show();   
        var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea&multiple=multiple";
        CallAJAX(url,"div_select_subarea");
    }else if(suburb_arr_count > 1){
        $("#subarea_div").show();   
        var multiple_id = suburb_str.join(", ");
        var url = "getSubareaDropdown.php?multiple_id="+multiple_id+"&select_name_id=subarea&multiple=multiple";
        CallAJAX(url,"div_select_subarea");
    }else{
        setSelectNoValue("div_select_subarea", "subarea");
    }       
}

function checkAvailability() {
    $('#addform').parsley().validate();
    var email = $("#email").val();
    var username = $("#username").val();
    var validate_user = 1;
    if(username != '')
    {
        validate_user = 1;
        jQuery.ajax({
            url: "../includes/checkUserAvailable.php",
            data:'validation_field=username&username='+username,
            type: "POST",
            async:false,
            success:function(data){     
                if(data=="exist") {
                    alert('Username already exists.');
                    validate_user = 1;
                } else {
                    validate_user = 0;                                       
                }
            },
            error:function (){}
        });
    }
    
    if(email != '')
    {
        validate = 1;
        jQuery.ajax({
            url: "../includes/checkUserAvailable.php",
            data:'validation_field=email&email='+email,
            type: "POST",
            async:false,
            success:function(data){         
                if(data=="exist") {
                    alert('E-mail already exists.');
                    validate = 1;
                    return false;
                } else {
                    validate = 0;
                }
            },
            error:function (){}
        });
    } 
    else if(username != '' && validate_user == 0)
    {
        validate = 0;
    }
    else 
    {
        validate = 1;
    }
    
    if(validate == 0 && validate_user == 0)
    {
        var action = $('#hidAction').val();
        $('#addform').attr('action', action);                   
        $('#hidbtnsubmit').val("submit");
        $('#addform').submit();
    }   
}
</script> 
<style>
.field-icon {
  float: right;
  margin-right:10px;
  margin-top: -30px;
  position: relative;
  z-index: 2;
}
</style>
<style>
.form-horizontal { font-weight:normal; }
</style>
</body>
<!-- END BODY -->
</html>
<!-- <script>
function fnShowStockist1(id){
    alert('hi');
    alert(id.value);
    if (window.XMLHttpRequest)
    {
        xmlhttp=new XMLHttpRequest();
    }
    else
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value+"&multiple=multiple&user_type_manage=sales",true);
    xmlhttp.send();
}
</script> -->



<!--  <script type="text/javascript">
  function showDiv(elem)
  {

      if(elem.value == 'Distributor')
      {
       // alert('Superstockist');
         document.getElementById('hidden_div').style.display = "block";
         document.getElementById('hidden_div1').style.display = "none";
         document.getElementById('hidden_div2').style.display = "none";
         document.getElementById('Subcategory').style.display = "none";
      }
      if(elem.value == 'Superstockist')
      {
         document.getElementById('hidden_div').style.display = "none";
         document.getElementById('hidden_div1').style.display = "none";
         document.getElementById('hidden_div2').style.display = "none";
         document.getElementById('Subcategory').style.display = "none";
      }
       if(elem.value == 'SalesPerson')
      {
         document.getElementById('hidden_div').style.display = "none";
         document.getElementById('hidden_div1').style.display = "block";
         document.getElementById('hidden_div2').style.display = "block";
         document.getElementById('Subcategory').style.display = "block";
      }
     
      if(elem.value == 'DeliveryPerson')
      {
         document.getElementById('hidden_div').style.display = "none";
         document.getElementById('hidden_div1').style.display = "block";
         document.getElementById('hidden_div2').style.display = "block";
          document.getElementById('Subcategory').style.display = "block";
      }
      if(elem.value == 'Accountant')
      {
         document.getElementById('hidden_div').style.display = "none";
         document.getElementById('hidden_div1').style.display = "none";
         document.getElementById('hidden_div2').style.display = "none";
         document.getElementById('Subcategory').style.display = "none";
      }
      if(elem.value == 'DeliveryChannelPerson')
      {
         document.getElementById('hidden_div').style.display = "block";
         document.getElementById('hidden_div1').style.display = "none";
         document.getElementById('hidden_div2').style.display = "none";
         document.getElementById('Subcategory').style.display = "none";
      }
      
    }
</script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>" type="text/javascript"></script>
<script type="text/javascript">
$( "#getlatlong" ).click(function() {
  //alert( "Handler for .click() called." );
  //$(this).find("option:selected").text();
  var curaddress=$('[name="address"]').val();
  var subarea=$('#subarea').val();
  if(subarea!=''){subarea=", "+$('#subarea').find("option:selected").text();}else{subarea='';}
   var area=$('#area').val();
  if(area!=''){area=", "+$('#area').find("option:selected").text();}else{area='';}
   var city=$('[name="city"]').val();
  if(city!=''){city=", "+$('[name="city"]').find("option:selected").text();}else{city='';}
   var state=$('[name="state"]').val();
  if(state!=''){state=", "+$('[name="state"]').find("option:selected").text();}else{state='';}
    var address = curaddress+''+subarea+''+ area+''+city+''+state;  
                    //alert(address);
    //var address123 = address.replace("/^[a-zA-Z\s](\d)?$/","");
    //alert(address123);
                    
  var geocoder = new google.maps.Geocoder();
    //var address = "new york";

    geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();     
        $('[name="latitude"]').val(latitude);
        $('[name="longitude"]').val(longitude);
      } else{
          alert("Not getting proper latitude,longitude!");
      }
    }); 
});

</script>