<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
} 
//include "../includes/userManage.php";
//$userObj 	= 	new userManager($con,$conmain);

include "../includes/commonuserManage.php";	
$userObj 	= 	new commonuserManager($con,$conmain);

$del_id = $_GET['del_id'];
if (isset($del_id) && $del_id!='') 
{
	$user_id = $del_id;
	        $userObj->deleteCommonUserDetails($user_id);
			$userObj->deleteAllInstanceCommonUser($user_id);
			echo '<script>alert("User Deleted successfully.");location.href="common-user.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	 $activeMainMenu = "ManageSupplyChain"; $activeMenu = "Common User";
    include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Common User</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Common User</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Common User Listing
							</div>
                            <a href="common-user-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Common User
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									User Type
								</th>
								<th>
									 Name
								</th>								
                                <th>
									 Email
								</th>
                                <th>
									 Mobile Number
								</th>
                                <th>
                                	District
                                </th>
                                <th>
                                  State
                                </th>
									<?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") 
								{ ?>
								<th>
                                  Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
						<?php
						$result1 = $userObj->getAllCommonUserInstanceDetails();
						//$result1 = mysqli_query($con,$sql);
						while($row = mysqli_fetch_array($result1))
						{
							//print_r($row);							
							echo '<tr class="odd gradeX">
								<td>'.fnStringToHTML($row['user_type']).'</td>
								<td><a href="common-user-update.php?utype='.$row['user_type'].'&id='.$row['common_user_id'].'">'.fnStringToHTML($row['firstname']).'</a></td>';
                              
								echo '<td>'.fnStringToHTML($row['email']).'</td>
                                <td>'.fnStringToHTML($row['mobile']).'</td>
								<td>'.fnStringToHTML($row['cityname']).'</td>
								<td>'.fnStringToHTML($row['statename']).'</td>';								
								if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {
									echo '<td>
										<a onclick="return checkDelete()" href="common-user.php?del_id='.$row['common_user_id'].'">Delete</a>
									</td>';
								}
								echo '</tr>';
							} ?>
						
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>

<script language="JavaScript" type="text/javascript">
function checkDelete(){
    return confirm('Are you sure you want to delete this User?');
}
</script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>