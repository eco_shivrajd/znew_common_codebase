<!-- BEGIN HEADER -->
<?php
include "../includes/grid_header.php";
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
include "../includes/productManage.php";
$userObj = new userManager($con, $conmain);
$orderObj = new orderManage($con, $conmain);
$shopObj = new shopManager($con, $conmain);
$prodObj = new productManage($con, $conmain);
$user_type = $_SESSION[SESSION_PREFIX . 'user_type'];
$userid = $_SESSION[SESSION_PREFIX . 'user_id'];
?>
<!-- BEGIN PAGE HEADER-->
</head>
<!-- END HEAD -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLEAPIKEY; ?>&callback=initMap" type="text/javascript"></script>

<body class="page-header-fixed page-quick-sidebar-over-content ">
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?php
        $activeMainMenu = "Orders";
        $activeMenu = "DirectOrdersFromstockist";
        include "../includes/sidebar.php";
        ?>
        <!-- END SIDEBAR -->
        <div class="page-content-wrapper">
            <div class="page-content">			
                <h3 class="page-title">
                    Manage Orders
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">					
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Orders</a>
                        </li>
                    </ul>				
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue-steel">
                            <div class="portlet-title">
                                <div class="caption">
                                    Manage Orders 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">						
                                <form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <label class="col-md-3">Order Status:</label>
                                        <div class="col-md-4">
                                            <select name="order_status" id="order_status" data-parsley-trigger="change" class="form-control">
                                                <?php if ($user_type == 'Distributor') { ?>
                                                    <option value="11" selected>Received By Sales Person</option>								
                                                    <option value="12">Placed Order To Superstockist</option>
                                                <?php } else if ($user_type == 'Superstockist') { ?>
                                                    <option value="12" selected>Received By Stockist</option>								
                                                    <option value="13">Placed Order To Manufacturer</option>
                                                <?php } else { ?>
                                                    <option value="43" selected>Delivered</option>
                                                    <option value="63" >Payment Done</option>
                                                    <option value="73" >Payment Pending</option>
                                                    <option value="83" >Partial Payment Done</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->	

                                    <div class="form-group" id="divDaily">
                                        <label class="col-md-3">Select Order Date:</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->								 
                                        </div>
                                    </div><!-- /.form-group -->							
                                    <div class="form-group">
                                        <label class="col-md-3">Sales Person:</label>
                                        <?php $user_result = $userObj->getAllLocalUser('salesperson'); ?>		
                                        <div class="col-md-4" id="divsalespersonDropdown">
                                            <select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_user = mysqli_fetch_assoc($user_result)) {
                                                    ?>									
                                                    <option value="<?= $row_user['id']; ?>"><?= $row_user['firstname']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->


                                    <div class="form-group">
                                        <label class="col-md-3">State:</label>
                                        <div class="col-md-4">
                                            <select name="dropdownState" id="dropdownState"              
                                                    data-parsley-trigger="change"				
                                                    data-parsley-required="#true" 
                                                    data-parsley-required-message="Please select state"
                                                    class="form-control" onChange="fnShowCity(this.value)">
                                                <option selected disabled>-select-</option>
                                                <?php
                                                $sql = "SELECT id,name FROM tbl_state where country_id=101 order by name";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    $cat_id = $row['id'];
                                                    echo "<option value='$cat_id'>" . $row['name'] . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>			
                                    <div class="form-group" id="city_div" style="display:none;">
                                        <label class="col-md-3">City:</label>
                                        <div class="col-md-4" id="div_select_city">
                                            <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>										
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group" id="area_div" style="display:none;">
                                        <label class="col-md-3">Region:</label>
                                        <div class="col-md-4" id="div_select_area">
                                            <select name="area" id="area" data-parsley-trigger="change" class="form-control">
                                                <option selected value="">-Select-</option>									
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="shop_div">
                                        <label class="col-md-3">Shops:</label>
                                        <div class="col-md-4">
                                            <?php $shop_result = $shopObj->getAllShops(); ?>									
                                            <select name="divShopdropdown" id="divShopdropdown" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_shop = mysqli_fetch_assoc($shop_result)) {
                                                    ?>									
                                                    <option value="<?= $row_shop['id']; ?>"><?= $row_shop['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Category:</label>
                                        <div class="col-md-4" id="divCategoryDropDown">
                                            <?php $cat_result = $prodObj->getAllCategory(); ?>
                                            <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
                                                <option value="">-Select-</option>
                                                <?php while ($row_cat = mysqli_fetch_assoc($cat_result)) {
                                                    ?>									
                                                    <option value="<?= $row_cat['id']; ?>"><?= $row_cat['categorynm']; ?></option>
                                                <?php } ?>								
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="col-md-3">Product:</label>
                                        <div class="col-md-4" id="divProductdropdown">
                                            <?php $prod_result = $prodObj->getAllProducts(); ?>
                                            <select name="dropdownProducts" id="dropdownProducts" class="form-control">
                                                <option value="">-Select-</option>
                                                <?php while ($row_prod = mysqli_fetch_assoc($prod_result)) {
                                                    ?>									
                                                    <option value="<?= $row_prod['id']; ?>"><?= $row_prod['productname']; ?></option>
                                                <?php } ?>	
                                            </select>
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-3">
                                            <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
                                            <button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </form>	
                                <div>
                                    <?php if ($user_type != 'Accountant') { ?>
                                        <button type="button" class="btn btn-success" id="statusbtnplacedorder" name="statusbtnplacedorder" data-toggle="modal">Place Orders</button>
                                    <?php } ?>
                                    <?php if ($user_type == 'Accountant') { ?>
                                        <button  type="button" class="btn btn-success" id="statusDbtn" name="statusDbtn" data-toggle="modal">Update Payment</button>
                                    <?php } ?>
                                </div><br>
                                <form class="form-horizontal" role="form" name="form" method="post" action="">	
                                    <div id="order_list">
                                    





<?php
$order_status = 1;
    //echo $order_status;
   /* $orders = $orderObj->getDirectOrdersFromStockist($order_status);
    print"<pre>";print_r($orders);
    exit();*/
            $orderscount = $orderObj->getStockistOrdersCount($order_status);
            $order_count = count($orderscount);
?>

                                         <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
<tr id="main_th">                         <?php 
if (($user_type == 'Superstockist' && $order_status == '1') && $order_count != 0) {//6 is payment done & 2 is assigned for delivery  ?>
<th id="select_th">
<input type="checkbox" name="select_all[]" id="select_all" disabled onchange="javascript:checktotalchecked(this)">
</th>
<?php
}
?>                    
                                                    <th>
                                                        Order Id<br><?php echo str_repeat("&nbsp;", 8); ?>Order Date 
                                                    </th>                               

                                                    <th>
                                                        Product Name            
                                                    </th>                                                   
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Total Price (₹)
                                                    </th>
                                                    <th>
                                                        GST Price (₹)
                                                    </th>
                                                    <th>
                                                        Action  
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
            <?php             
         //  echo $order_count."ppp";
            // echo "<pre>";print_r($orderscount);exit();
            foreach($orderscount as $key => $value) {
                $orderdetails=array();
                $orderdetails = $orderObj->getStockistPlacedOrders($value);
                $order_no=$orderdetails[0]['order_no'];
                $order_date=$orderdetails[0]['order_date'];
                 $product_name='';
                  $product_variant='';
                   $prod_qnty='';
                    $totalcost='';
                     $gstcost='';               
                    foreach ($orderdetails as $key1 => $value1) {      
                    // echo "<pre>";print_r($orderdetails);
                        $product_name.=$value1['product_name']."(".$value1['product_variant'].")<br>";
                        $product_variant.=$value1['product_variant']."<br>";                                                            
                        $prod_qnty.="".$value1['product_quantity']."<br>";
                        $totalcost.="".$value1['total_cost']."<br>";
                        $gstcost.="".$value1['p_cost_totalwith_tax']."<br>";
                       // $order_no.="".$value1['order_no'];
                    }
              
                ?>
                <tr class="odd gradeX">
                    <?php if (($user_type == 'Superstockist' && $order_status == '1') && $order_count != 0) { ?>
                                                            <td><input type="checkbox" name="select_all[]" id="select_all" value="<?php echo $order_no; ?>"  onchange="javascript:checktotalchecked(this)">
                                                            <?php } ?>
                                                        </td>


                    <td ><font size="1.4"><?php echo $order_no; ?></font><br><?php echo date('d-m-Y H:i:s', strtotime($order_date)); ?></td>                                        
                    <td><?php echo $product_name; ?></td>                                                     
                    <td align="right"><?php echo $prod_qnty; ?></td>
                    <td align="right"><?php echo $totalcost; ?></td>
                    <td align="right"><?php echo $gstcost; ?></td>
                    <td>
                        <a onclick="showInvoice(1,<?= $order_no; ?>)" title="View Invoice">View Invoice</a>
                    </td>
                </tr>
            <?php } ?>

                                            </tbody>
                                        </table>
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>

                </div>
                <!-- END PAGE CONTENT-->
                <div class="modal fade" id="view_invoice" role="dialog">
                    <div class="modal-dialog" style="width: 980px !important;">    
                        <!-- Modal content-->
                        <div class="modal-content" id="view_invoice_content">      
                        </div>      
                    </div>
                </div>
                <div class="modal fade" id="order_details" role="dialog">
                    <div class="modal-dialog" style="width: 880px !important;">
                        <!-- Modal content-->
                        <div class="modal-content" id="order_details_content">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="model_content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        </div>
                        <div class="modal-body" style="padding-bottom: 5px !important;"> 
                            <div id="map" style="width: 100%; height: 500px;"></div> 
                        </div>
                    </div>
                </div>
            </div>

            <!--New pop up for accountant payment status-->
            <div class="modal fade" id="assign_delivered" role="dialog" style="height:auto;">
                <div class="modal-dialog">    
                    <!-- Modal content-->
                    <div class="modal-content" style="height:270px;" >
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Payment Update</h4>
                        </div>
                        <div class="modal-body" style="padding-bottom: 5px !important;">
                            <form class="form-horizontal">
                                <input type="hidden" value="" id="orderids4" name="orderids4">

                                <div class="form-group">
                                    <label class="col-md-6">Order Status:</label>
                                    <div class="col-md-6">
                                        <select name="order_status_payment" id="order_status_payment" data-parsley-trigger="change" class="form-control">
                                            <option value="6" selected>Payment Done</option>								
                                            <option value="7" selected>Payment Pending</option>	
                                            <option value="8" selected>Partial Payment Done</option>									
                                        </select>
                                    </div>
                                </div><!-- /.form-group -->	
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" name="btn_delivered" id="btn_delivered" class="btn btn-primary" data-dismiss="modal">Assign</button>

                                    </div>
                                </div>
                            </form>
                        </div>	
                    </div>
                </div>
            </div>


            <!-- BEGIN FOOTER -->
            <?php include "../includes/grid_footer.php" ?>
            <!-- END FOOTER -->
            <!-- START JAVASCRIPTS -->
            <script>
                $(document).ready(function () {
                    $("#select_th").removeAttr("class");
                    $("#main_th th").removeAttr("class");
                    if ($.fn.dataTable.isDataTable('#sample_2')) {
                        table = $('#sample_2').DataTable();
                        table.destroy();
                        table = $('#sample_2').DataTable({
                            "aaSorting": [],
                        });
                    }
                });
                $('.date-picker1').datepicker({
                    rtl: Metronic.isRTL(),
                    orientation: "left",
                    endDate: "<?php echo date('d-m-Y'); ?>",
                    autoclose: true
                });

                function fnReset() {
                    location.reload();
                }
                //Payment done or not for accountant login
                $("#btn_delivered").click(function () {
                    var orderids4 = $("#orderids4").val();
                    var order_status_payment = $("#order_status_payment").val();
                    var url = 'updateOrderDelivery.php?flag=updatepayment&orderids=' + orderids4 + '&order_status_payment=' + order_status_payment;
                    $.ajax({
                        url: url,
                        datatype: "JSON",
                        contentType: "application/json",

                        error: function (data) {
                            console.log("error:" + data)
                        },
                        success: function (data) {
                            console.log(data);
                            if (data > 0) {
                                alert("Orders Payment Status Updated!");
                                location.reload();
                            } else {
                                alert("Not updated");
                            }
                        }
                    });
                });
                $("#statusDbtn").click(function () {
                    var void1 = [];
                    $('input[name="select_all[]"]:checked').each(function () {
                        void1.push(this.value);
                    });
                    var energy = void1.join();
                    //alert(energy);alert("fgdfg");
                    if (energy != '') {
                        $("#assign_delivered").modal('show');
                        $("#orderids4").val(energy);
                    } else {
                        alert("Please select minimum one order");
                        //location.reload();
                    }
                });
                /*function fnShowCity(id_value) {	
                 //alert(id_value);
                 $("#city_div").show();	
                 $("#area").html('<option value="">-Select-</option>');	
                 $("#subarea").html('<option value="">-Select-</option>');	
                 var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
                 CallAJAX(url,"div_select_city");	
                 FnGetShopsDropdown("");
                 }*/
                function fnShowCity(id_value) {
                    $("#city_div").show();
                    $("#area").html('<option value="">-Select-</option>');
                    $("#subarea").html('<option value="">-Select-</option>');
                    var url = "getCityDropDown.php?cat_id=" + id_value + "&select_name_id=city&mandatory=mandatory";
                    CallAJAX(url, "div_select_city");
                }
                function FnGetSuburbDropDown(id) {
                    $("#area_div").show();
                    $("#subarea").html('<option value="">-Select-</option>');
                    var url = "getSuburDropdown.php?cityId=" + id.value + "&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
                    CallAJAX(url, "div_select_area");
                }
                /*function FnGetSuburbDropDown(id) {
                 $("#area_div").show();	
                 $("#subarea").html('<option value="">-Select-</option>');		
                 var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetShopsDropdown";
                 CallAJAX(url,"div_select_area");
                 FnGetShopsDropdown("");
                 }*/
                function FnGetShopsDropdown(id) {
                    $("#shop_div").show();
                    $("#divShopdropdown").html('<option value="">-Select-</option>');
                    var param = "";
                    var state_id = $("#dropdownState").val();
                    var city_id = $("#city").val();
                    var suburb_id = $("#area").val();
                    if (state_id != '')
                        param = param + "&state_id=" + state_id;
                    if (city_id != '')
                        param = param + "&city_id=" + city_id;
                    if (suburb_id != '') {
                        if (suburb_id != undefined) {
                            param = param + "&suburb_id=" + suburb_id;
                        }
                    }
                    if (id != '') {
                        if (id != undefined)
                            param = param + "&suburb_id=" + id.value;
                    }

                    var url = "getShopDropdownByAddress.php?param=param" + param;
                    CallAJAX(url, "divShopdropdown");
                }
                function fnShowProducts(id) {
                    var url = "getProductDropdown.php?cat_id=" + id.value;
                    CallAJAX(url, "divProductdropdown");
                }
                $("#select_all").click(function () {
                    $('input:checkbox').prop('checked', this.checked);
                });
                function checktotalchecked(obj) {
                    $("#select_th").removeAttr("class");
                    if ($(obj).is(':checked') == true)
                    {
                        var checkbox_count = ($('input:checkbox').length) - 1;
                        var check_count = ($('input:checkbox:checked').length);
                        if (obj.value == '') {
                            $('input:checkbox').attr('checked', true);
                        } else if (checkbox_count == check_count)
                            $('input:checkbox').attr('checked', true);

                        /*if(obj.value != ''){
                         var check_value = obj.value;
                         var part = check_value.split("_");alert(part[0] );alert('^ordersub_'+part[1]+'_');
                         if(part[0] == 'ordermain'){
                         $(":checkbox[value='/^ordersub_"+part[1]+"_[0-9]{10}/']").prop("checked","true");
                         }
                         }*/
                    } else {
                        $('#select_all').attr('checked', false);
                        if (obj.value == '')
                            $('input:checkbox').attr('checked', false);
                    }
                }
                function ShowReport() {
                    //alert("sdfsdf");
                    var order_status = $('#order_status').val();
                    var usertype =<?php echo "'" . $user_type . "'"; ?>;
                    //alert(usertype);
                    if (usertype == 'Superstockist' && order_status == '1') {
                        $("#statusbtnplacedorder").show();
                    } else {
                        $("#statusbtnplacedorder").hide();
                    }

                    var url = "ajax_show_orders.php";
                    var data = $('#frmsearch').serialize();
                    jQuery.ajax({
                        url: url,
                        method: 'POST',
                        data: data,
                        async: false
                    }).done(function (response) {
                        $('#order_list').html(response);
                        var table = $('#sample_2').dataTable();
                        table.fnFilter('');
                        $("#select_th").removeAttr("class");
                    }).fail(function () { });
                    return false;
                }
                // Change_supp_chnz_status() 
                $("#statusbtnplacedorder").click(function () {
                    var void1 = [];
                    $('input[name="select_all[]"]:checked').each(function () {
                        void1.push(this.value);
                    });
                    var energy = void1.join();
                    //alert(energy);

                    if (energy != '') {
                        //$("#orderids").val(energy);	

                       var url = 'updateDirectOrderDelivery.php?flag=PlaceOrder&orderids=' + energy;

                        $.ajax({
                            url: url,
                            datatype: "JSON",
                            contentType: "application/json",

                            error: function (data) {
                                console.log("error:" + data)
                            },
                            success: function (data) {
                                console.log('hi');
                                console.log(data);
                                if (data > 0) {
                                    alert("Order Placed Suceessfully");
                                    location.reload();
                                } else {
                                    alert("Not updated.");
                                }
                            }
                        });
                    } else {
                        alert("Please select minimum one order");
                        return false;
                    }

                });

                function showInvoice(order_status, id) {
                    var url = "invoice2.php";
                    jQuery.ajax({
                        url: url,
                        method: 'POST',
                        data: 'order_status=' + order_status + '&order_id=' + id,
                        async: false
                    }).done(function (response) {
                        $('#view_invoice_content').html(response);
                        $('#view_invoice').modal('show');
                    }).fail(function () { });
                    return false;
                }
                function showOrderDetails(id, order_type) {
                    var url = "order_details_popup.php";
                    jQuery.ajax({
                        url: url,
                        method: 'POST',
                        data: 'order_details_id=' + id + '&order_type=' + order_type,
                        async: false
                    }).done(function (response) {
                        $('#order_details_content').html(response);
                        $('#order_details').modal('show');
                    }).fail(function () { });
                    return false;
                }
                function takeprint() {
                    var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                    var divContents = '<style>\body {\
                            font-size: 12px;}\
                    th {text-align: left;}\
                    .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
                    table { border-collapse: collapse;  \
                            font-size: 12px; }\
                    table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
                    body { font-family: "Open Sans", sans-serif;\
                    background-color:#fff;\
                    font-size: 15px;\
                    direction: ltr;}</style>' + $("#divPrintArea").html();
                    if (isIE == true) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(divContents);
                        printWindow.focus();
                        printWindow.document.execCommand("print", false, null);
                    } else {
                        $('<iframe>', {
                            name: 'myiframe',
                            class: 'printFrame'
                        }).appendTo('body').contents().find('body').html(divContents);
                        window.frames['myiframe'].focus();
                        window.frames['myiframe'].print();
                        setTimeout(
                                function ()
                                {
                                    $(".printFrame").remove();
                                }, 1000);
                    }
                }
                function takeprint11() {
                    var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                    var divContents = '<style>\body {\
                            font-size: 12px;}\
                    th {text-align: left;}\
                    .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
                    table { border-collapse: collapse;  \
                            font-size: 12px; }\
                    table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
                    body { font-family: "Open Sans", sans-serif;\
                    background-color:#fff;\
                    font-size: 15px;\
                    direction: ltr;}</style>' + $("#divPrintArea").html();
                    if (isIE == true) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(divContents);
                        printWindow.focus();
                        printWindow.document.execCommand("print", false, null);
                    } else {
                        $('<iframe>', {
                            name: 'myiframe',
                            class: 'printFrame'
                        }).appendTo('body').contents().find('body').html(divContents);
                        window.frames['myiframe'].focus();
                        window.frames['myiframe'].print();
                        setTimeout(
                                function ()
                                {
                                    $(".printFrame").remove();
                                }, 1000);
                    }
                }
                function OrderDetailsPrint() {
                    var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                    var divContents = '<style>\body {\
                            font-size: 12px;}\
                    th {text-align: left;}\
                    .printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
                    table { border-collapse: collapse;  \
                            font-size: 12px; }\
                    table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
                    body { font-family: "Open Sans", sans-serif;\
                    background-color:#fff;\
                    font-size: 15px;\
                    direction: ltr;}</style>' + $("#divOrderPrintArea").html();
                    if (isIE == true) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(divContents);
                        printWindow.focus();
                        printWindow.document.execCommand("print", false, null);
                    } else {
                        $('<iframe>', {
                            name: 'myiframe',
                            class: 'printFrame'
                        }).appendTo('body').contents().find('body').html(divContents);
                        window.frames['myiframe'].focus();
                        window.frames['myiframe'].print();
                        setTimeout(
                                function ()
                                {
                                    $(".printFrame").remove();
                                }, 1000);
                    }
                }
                function takeprint_invoice() {
                    var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
                    var divContents = '<style>\
                    .darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
                    .fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
                    .fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
                    .font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
                    .font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
                    .table-bordered-popup {    border: 1px solid #364622;}\
                    .table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
                            border: 1px solid #364622;	color:#4a5036;}\
                    .blue{	color:#010057;}\
                    .blue1{	color:#574960;	font-size:16px;}\
                    .buyer_section{	color:#574960;	font-size:14px;}\
                    .pad-5{	padding-left:10px;}\
                    .pad-40{	padding-left:40px;}\
                    .np{	padding-left:0px;	padding-right:0px;}\
                    .bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
                     background-size: 200px 200px;}\
                    </style>' + $("#divPrintArea").html();
                    if (isIE == true) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(divContents);
                        printWindow.focus();
                        printWindow.document.execCommand("print", false, null);
                    } else {
                        $('<iframe>', {
                            name: 'myiframe',
                            class: 'printFrame'
                        }).appendTo('body').contents().find('body').html(divContents);
                        window.frames['myiframe'].focus();
                        window.frames['myiframe'].print();
                        setTimeout(
                                function ()
                                {
                                    $(".printFrame").remove();
                                }, 1000);
                    }
                }
                var lat = 0;
                var lng = 0;

                $('#googleMapPopup').on('shown.bs.modal', function (e) {

                    var latlng = new google.maps.LatLng(lat, lng);
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: latlng,
                        zoom: 17
                    });
                    var marker = new google.maps.Marker({
                        map: map,
                        position: latlng,
                        draggable: false,
                        //anchorPoint: new google.maps.Point(0, -29)
                    });
                });
                function showGoogleMap(getlat, getlng) {

                    lat = getlat;
                    lng = getlng;
                    $('#googleMapPopup').modal('show');
                }

            </script>
            <!-- END JAVASCRIPTS -->
            </body>
            <!-- END BODY -->
            </html>
            </html>