<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
if (isset($_POST['submit'])) {
    $level = $_POST['level'];
    $usertype = $_POST['usertypeselect'];
    $margin = $_POST['margin'];    
    $added_on = date("Y-m-d H:i:s");
	  $sql_user_type_check="select id as userlevelid from `tbl_userlevel` where usertype='$usertype'";
	$sql_usertype_check = mysqli_query($con, $sql_user_type_check);
    if ($rowcount = mysqli_num_rows($sql_usertype_check) > 0) 
    {
		$row_ids = mysqli_fetch_array($sql_usertype_check);
		//echo "<pre>";print_r($row_ids);
		 $update_user_sql = "UPDATE tbl_userlevel SET margin='$margin',last_updated_on=now() 
		WHERE id='".$row_ids['userlevelid']."'";	//die();	
        $sql_product_add = mysqli_query($con, $update_user_sql);      
        echo '<script>alert("User Type margin updated.");location.href="userlevel_list.php";</script>';
    } 
    else 
    {
        $sqlprod = "INSERT INTO `tbl_userlevel` (level,usertype,margin,ul_added_on) 
		VALUES('$level','$usertype','$margin',now())";
		
        $sql_product_add = mysqli_query($con, $sqlprod);        
        echo '<script>alert("User level added successfully.");location.href="userlevel_list.php";</script>';
    }
    //exit;
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "UserLevel";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->			
			<h3 class="page-title">
			User Level Margin
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">User Level Margin</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">    
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Level Margin Listing
							</div>
                           
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_level">
							<thead>
							<tr>
								<th width="20%">
									 Level
								</th>	
								<th width="50%">
									Level Name/User Type
								</th>
								<th width="30%">
									Margin%
								</th>
                               
							</tr>
							</thead>
							<tbody>
							<?php							
							$result1 = $userObj->getAllLocalUserLevels();							
							$counter=1;
							while($row = mysqli_fetch_array($result1))
							{
								$id = $row['id'];
								$level = $row['level'];
								$usertype = $row['usertype'];
								$margin = $row['margin'];
							
							?>
							<tr class="odd gradeX">
								<td><?php echo $level; ?></td>
								<td><?php echo $usertype; ?></td>
								<td><?php echo round($margin,2)."%"; ?></td>
							</tr>	
							<?php $counter=$counter+1;}  ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12">    
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Level Margin Add
							</div>                           
						  <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span> 

							<form class="form-horizontal" data-parsley-validate="" id="myForm" name="myForm" role="form" method="post" action="userlevel_list.php" enctype="multipart/form-data">         

							
							<div class="form-group">
							  <label class="col-md-3">User Type Level<?php echo $counter;?>:<span class="mandatory">*</span></label>
							  <input type='hidden' name="level" value="level<?php echo $counter; ?>">
							  <div class="col-md-4">
								<select name="usertypeselect"
										data-parsley-trigger="change"				
										data-parsley-required="#true" 
										data-parsley-required-message="Please select user type"
										class="form-control">
									<option  selected disabled>-Select-</option>
									<?php
									$result2 = $userObj->getAllLocalUserTypes();								
									while($row2 = mysqli_fetch_array($result2))
									{
										$distinct_usertype = $row2['user_type'];
										echo "<option value='$distinct_usertype'>" . fnStringToHTML($distinct_usertype) .  "</option>";
									}
									?>
								</select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-md-3">Margin(%):<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<input type="text" name="margin" class="form-control" onkeyup="myFunction(this)"
								placeholder="Enter Margin"
								data-parsley-trigger="change"
								data-parsley-pattern="^(?!\s)[0-9-!@#$%^&*+_=><,./:' ]*$" required >
							  </div>
							</div>
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">									
									<button type="submit" name="submit" class="btn btn-primary" onclick="return chk_null();">Submit</button>
									<a href="userlevel_list.php.php" class="btn btn-primary">Cancel</a>
								</div>
							</div><!-- /.form-group --> 
						</form> 
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">

 function myFunction(varl) {
	if (varl.value != "") {
        var arrtmp = varl.value.split(".");
        if (arrtmp.length > 1) {
			var strTmp = arrtmp[1];
			if (strTmp.length > 2) {
				varl.value = varl.value.substring(0, varl.value.length - 1);
			}
        }
	}
}
</script>
</body>
<!-- END BODY -->
</html>