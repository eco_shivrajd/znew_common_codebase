<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include "../includes/grid_header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Leads";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Leads</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Leads</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Lead Listing</div>
							
						<!-- 	<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="leads-add.php" class="btn btn-sm btn-default pull-right mt5">Add Lead</a>
							<? } ?> -->
							
                            <div class="clearfix"></div>    
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											 Shop Name
										</th>
										<!-- <?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Superstockist'){ ?>
										<th>
											 Super Stockist
										</th>
										<?php } ?>
										<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
										<th>
											 Stockist
										</th>
										<?php } ?> -->
										<th>
											 Contact Person
										</th>
										<th>
											 Mobile Number
										</th>
										<th>
											Taluka
										</th>
										<th>
											District
										</th>
										<th>
											State
										</th>

										<th>
											Reminder
										</th>
										<th>
											Visited Date
										</th>
										<th>
											Comment
										</th>										
										<th>
										     Status
										</th>
										
									</tr>
								</thead>
							<tbody>
							<?php							
							// $sql = "SELECT * FROM `tbl_shop_view`";
							// $sql = "SELECT id,status,status_seen_log,sstockist,stockist,shop_name,contact_person,mobile,cityname,statename,suburbnm FROM `tbl_leads`";
					       $result1 = $shopObj->getShopLeads();
					       //var_dump($result1);
                            // $result = mysqli_query($con, $sql);
							while($row = mysqli_fetch_array($result1))
							{
							//echo "<pre>";
							//print_r($row);
                                    $shop_id = $row['id'];
                                    $notification = '';
                                    $visited_date = '';
                                    $reminder = '';
							         $sql1 = "SELECT `id`, `shop_id`,`reminder`,`visited_date`, `notification`, `created_on` FROM `tbl_lead_details` where `shop_id` = $shop_id";
							         $result2 = mysqli_query($con, $sql1);
							         while($row2 = mysqli_fetch_array($result2))
							         {	
							         	//print_r($row2);  
                                        $notification.=$row2['notification']."<br><br>";
                                        $visited_date.=$row2['visited_date']."<br><br>";
                                        $reminder.=$row2['reminder']."<br><br>";        
							         }	
							         $sql3 = "SELECT max(lead_status) as lead_status FROM `tbl_lead_details` where `shop_id` = $shop_id";
							         $result3 = mysqli_query($con, $sql3);
							         while($row3 = mysqli_fetch_array($result3))
							         {
							         	 $lead_status=$row3['lead_status'];
							         }
                           ?>
							<tr class="odd gradeX" >
								<td>
								 <a href="#"><?php echo $row['name'];?></a>
								</td> 
									<td><?php echo $row['contact_person'];?></td>
									<td><?php echo $row['mobile'];?></td> 
									<td><?php echo $row['area_name'];?></td> 
									<td><?php echo $row['city_name'];?></td> 
									<td><?php echo $row['state_name'];?></td> 

                                        <td>                                    	
                                    	<?php echo $reminder;?>	
                                    	</td> 
                                     <td><?php echo $visited_date;?></td> 
                                      <td><?php echo $notification;?></td> 
                                      <td>
                                      	<a href="#">
                                        <?php 
                                        if ($lead_status=='1') 
                                        {
                                        	echo "Pending";
                                        }
                                        elseif($lead_status=='2') 
                                        {
                                        	echo "Confirmed";
                                        }
                                        else
                                        {
                                        	echo "Cancelled";
                                        }                                       
                                        ?>	                                     	

                                      </a>
                                      </td> 
                               </tr>
											
                             <?php
							} 
							?>
	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>