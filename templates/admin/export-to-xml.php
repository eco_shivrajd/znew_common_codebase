<?php
include "../includes/config.php";
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

/** create XML file */ 
$mysqli = new mysqli('localhost', 'salzpoin_znew_ua', 'b#jc-uoUyyGQ', 'salzpoin_znew_uatkaysons');

/* check connection */
if ($mysqli->connect_errno) {
   echo "Connect failed ".$mysqli->connect_error;
   exit();
}

$query = "SELECT id, title, author_name, price, ISBN, category FROM books";

$booksArray = array();

if ($result = $mysqli->query($query)) {

    /* fetch associative array */
    while ($row = $result->fetch_assoc()) {
       array_push($booksArray, $row);
    }
  
    if(count($booksArray)){
         createXMLfile($booksArray);
     }

    /* free result set */
    $result->free();
}

/* close connection */
$mysqli->close();

function createXMLfile($booksArray){
  //echo "<pre>";print_r($booksArray);die();
   $filePath = 'book.xml';

   $dom     = new DOMDocument('1.0', 'utf-8'); 

   $root      = $dom->createElement('books'); 
	$dom->appendChild($root);
   for($i=0; $i<count($booksArray); $i++){     
     $bookId        =  $booksArray[$i]['id']; 
     $bookName      =  $booksArray[$i]['title'];
     $bookAuthor    =  $booksArray[$i]['author_name']; 
     $bookPrice     =  $booksArray[$i]['price']; 
     $bookISBN      =  $booksArray[$i]['ISBN']; 
     $bookCategory  =  $booksArray[$i]['category'];	
     $book = $dom->createElement('book');
	 $bookid     = $dom->createElement('id', $bookId); 
	 $book->appendChild($bookid);  
	 
     $name     = $dom->createElement('name', $bookName); 
	 $book->appendChild($name);  

     $author   = $dom->createElement('author', $bookAuthor);
     $book->appendChild($author); 

     $price    = $dom->createElement('price', $bookPrice); 
     $book->appendChild($price); 

     $isbn     = $dom->createElement('ISBN', $bookISBN); 
     $book->appendChild($isbn); 
     
     $category = $dom->createElement('category', $bookCategory); 
     $book->appendChild($category);
 
     $root->appendChild($book);
   }

   $dom->appendChild($root); 

   $dom->save($filePath); 
    $name123 = strftime('backup_%m_%d_%Y.xml');
    header('Content-Disposition: attachment;filename=' . $name123);
    header('Content-Type: text/xml');

    echo $dom->saveXML();

 } 