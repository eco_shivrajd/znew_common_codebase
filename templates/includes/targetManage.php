<?php
/***********************************************************
 * File Name	: shopManage.php
 ************************************************************/	

class targetManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	
	
	public function addTarget($user_type,$id) 
	{
		//print_r($_POST);
		//exit();	
		extract ($_POST);
		$added_by = $_SESSION[SESSION_PREFIX."user_id"];
		$assign_user_id = $id;

		if($user_type != '')
		{
			$fields.= ",`assign_user_type`";
			$values.= ",'".$user_type."'";
		}
		if($target_in_rs != '')
		{
			$fields.= ",`target_in_rs`";
			$values.= ",'".$target_in_rs."'";
		}
		if($targetType == 'daily')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$frmdate."'";
		}
		if($targetType == 'weekly')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$drpWeeklyOption."'";
		}
		if($targetType == 'monthly')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$drpMonthlyOption."'";
		}
		if($targetType == 'spdate')
		{
			$fields.= ",`start_date`";
			$values.= ",'".$frmdate."'";
			$fields.= ",`end_date`";
			$values.= ",'".$todate."'";
		}		

		$added_on = date('Y-m-d H:i:s');
		 $target_sql = "INSERT INTO tbl_target (`added_by`, `assign_user_id`, `target_type`, `createdon` $fields) 
		VALUES('".$added_by."','".$assign_user_id."','".$targetType."','".$added_on."' $values)";
		//exit();		
		mysqli_query($this->local_connection,$target_sql);
		//return $userid=mysqli_insert_id($this->common_connection); 
	}	
	public function getTargets() {
	  $sql1="SELECT `id`,`assign_user_type`, `target_type`,`start_date`,`end_date`,
		 (SELECT firstname FROM tbl_user WHERE id = tbl_target.assign_user_id) AS name,
		 `target_in_rs` FROM tbl_target";
		// exit();
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			//return $row = mysqli_fetch_assoc($result1);
			return $result1;		
		}else
			return $row_count;		
	}
	
}
?>