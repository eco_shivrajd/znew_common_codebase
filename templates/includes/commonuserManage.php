<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
error_reporting(E_ALL);
class commonuserManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) 
	{
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}
	public function getManufacturer()
	 {		
		$sql1="SELECT `id`, `clientnm`	FROM tbl_clients_maintenance where id!='".COMPID."' ORDER BY clientnm asc";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getCommonUserManufacturerId($common_user_id)
	 {			
		 $sql1="SELECT `common_user_manufacturers` FROM tbl_users where uid='$common_user_id'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	

	}
	public function getAllCommonUserDetails($common_user_id) {
		$sql1="SELECT `id`, `external_id`,`common_user_id`,`firstname`, `username`, 
		`pwd`, `user_type`, `address`, `subarea_ids`, `suburb_ids`,`city`, `state`, `mobile`,`office_phone_no`,`user_status`, `accname`,`accno`,`bank_name`,`accbrnm`,`accifsc`,`latitude`,`longitude`,
		`email`, `reset_key`, `is_default_user`, `sstockist_id`, `gst_number_sss`
		FROM tbl_user where common_user_id = '".$common_user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getAllCommonUserInstanceDetails(){	
	 // $sql1="SELECT `id`, `external_id`, `firstname`, `username`, `pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, `reset_key`, `suburb_ids`,`suburb_ids`,`is_default_user`, `is_common_user`, `sstockist_id` FROM tbl_user where tbl_user.is_common_user = '1' AND tbl_user.isdeleted!='1' order by is_default_user desc, firstname asc";

   $sql1="SELECT `id`,`common_user_id`,`external_id`, `firstname`, `user_type`, `cityname`, `statename`, `mobile`, `email` FROM common_user_view";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function addCommonUserForInstances($user_type) 
	{
		extract ($_POST);
        /*  echo "string";
		print_r($_POST);*/
		$common_user_manufacturers=implode(',', $manufacturer_id);	
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);

		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($firstname != '')
		{
			$fields.= ",`firstname`";
			$values.= ",'".$firstname."'";
		}
		if($common_user_manufacturers != '')
		{
			$fields.= ",`common_user_manufacturers`";
			$values.= ",'".$common_user_manufacturers."'";
		}
		$added_on = date('Y-m-d H:i:s');
		$is_common_user =1;
		$user_sql = "INSERT INTO tbl_users (`company_id`,`is_common_user`,`emailaddress`, `passwd`, `username`, `added_on`, `level` $fields) 
		VALUES('".COMPID."','".$is_common_user."','".$email."','".$password."','".$username."','".$added_on."','".$user_type."' $values)";		
		
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}

    public function addAllLocalUserForInstances($user_type,$common_user_id) {
		extract ($_POST);		
       // $external_id =	$_SESSION[SESSION_PREFIX.'user_id'];
		//print"<pre>";print_r($_POST);
      //  exit();	
		
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		$is_common_user =1;

		$fields = '';
		$values = ''; 
	

		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
		if($phone_no != '')
		{
			$fields.= ",`office_phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";			
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";			
			$values.= ",'".$accno."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";			
			$values.= ",'".$bank_name."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";			
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";			
			$values.= ",'".$accifsc."'";
		}
		if($latitude != '')
		{
			$fields.= ",`latitude`";
			$values.= ",'".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{
			$fields.= ",`longitude`";
			$values.= ",'".fnEncodeString($longitude)."'";
		}
		$added_on = date('Y-m-d H:i:s');
		   if($user_type == 'Distributor' || $user_type == 'SalesPerson' || $user_type == 'DeliveryPerson')
              {
							$user_sql14 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' AND `is_default_user`='1' LIMIT 1";
							$result14 = mysqli_query($this->local_connection,$user_sql14);
							$count = mysqli_num_rows($result14);
							if ($count>0)
							{
								while($row14 = mysqli_fetch_array($result14))
								{
								// $userid1 = $row['id'];
								$supp_external_id_new  =$row14['id'];
								//	echo $external_id_new.'--';
								$supp_sstockist_id  =$row14['id'];
								}
							}
							else
							{
								$user_sql15 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' LIMIT 1";
								$result15 = mysqli_query($this->local_connection,$user_sql15);
								while($row15 = mysqli_fetch_array($result15))
								{
								$supp_external_id_new  =$row15['id'];
								$supp_sstockist_id  =$row15['id'];
								}
							} 
				}
				if($user_type == 'Superstockist' || $user_type == 'DeliveryChannelPerson')
                {
                 	$supp_external_id_new =1;
                 	$supp_sstockist_id =0;
				}
		
	   $user_sql = "INSERT INTO tbl_user (`common_user_id`,`added_on`,`external_id`,`is_common_user`,`firstname`,`username`,`pwd`,`sstockist_id`,`user_type` $fields) 
		VALUES('".$common_user_id."','".$added_on."','".$supp_external_id_new."','".$is_common_user."','".$name."','".$username."','".$password."','".$supp_sstockist_id."','".$user_type."' $values)";		
		mysqli_query($this->local_connection,$user_sql);


		 $constring = array("1"=>array("dbname"=>"salzpoin_znew_uatsaras","dbuser"=>"salzpoin_znew_ua","pwd"=>"b#jc-uoUyyGQ"), "7"=>array("dbname"=>"salzpoin_znew_uatkaysons","dbuser"=>"salzpoin_znew_ua","pwd"=>"b#jc-uoUyyGQ"));

			foreach ($manufacturer_id as $key => $value)
			{
		         $dbname =  $constring[$value]['dbname'];
		         $dbuser = $constring[$value]['dbuser'];
		         $pwd = $constring[$value]['pwd'];
		      	 $con5	=	mysqli_connect('localhost', $dbuser, $pwd, $dbname) or die("cannot connect");

                 if($user_type == 'Distributor')
                {
							$user_sql11 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' AND `is_default_user`='1' LIMIT 1";
							$result17 = mysqli_query($con5,$user_sql11);
							$count = mysqli_num_rows($result17);
							if ($count>0)
							{
								while($row17 = mysqli_fetch_array($result17))
								{
								// $userid1 = $row['id'];
								$external_id_new  =$row17['id'];
								//	echo $external_id_new.'--';
								$sstockist_id  =$row17['id'];
								}
							}
							else
							{
								$user_sql11 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' LIMIT 1";
								$result1 = mysqli_query($con5,$user_sql11);
								while($row = mysqli_fetch_array($result1))
								{
								$external_id_new  =$row['id'];
								$sstockist_id  =$row['id'];
								}
							} 
				}
				if($user_type == 'Superstockist')
                {
                 	$external_id_new =1;
                 	$sstockist_id =0;
				}               

		         $user_sql1 = "INSERT INTO tbl_user (`common_user_id`,`external_id`,`is_common_user`,`firstname`,`username`,`pwd`,`sstockist_id`,`user_type` $fields) 
				  VALUES('".$common_user_id."','".$external_id_new."','".$is_common_user."','".$name."','".$username."','".$password."','".$sstockist_id."','".$user_type."' $values)";
				  mysqli_query($con5,$user_sql1);
		        mysqli_query($this->common_connection,$company_sql);
			}	
		//$userid=mysqli_insert_id($this->local_connection);
		$this->commonObj->log_add_record('tbl_user',$common_user_id,$user_sql);	
		//return $userid;
	}

  public function updateInstanceCommonUserDetails($common_user_id) {		
		extract ($_POST);	
		if($firstname != '')
		{
			$values.= "`firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `emailaddress`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		$update_user_sql = "UPDATE tbl_users SET $values WHERE uid='$common_user_id'";
		mysqli_query($this->common_connection,$update_user_sql);		
	}

	public function updateAllInstanceCommonUser($user_id) {		
		extract ($_POST);	
		//print_r($_POST);
		$old_manufacturer_id = explode(',', $old_manufacturer_id);
	    $user_type = $user_type1;
		//var_dump($old_manufacturer_id);
		
		
		//echo '--'.$user_id;

          //For insert
		$firstname_in = $firstname;
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
       $is_common_user = 1;
       	// for update instance
		if($firstname != '')
		{
			$values1.= "`firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values1.= ", `username`= '".fnEncodeString($username)."'";
			$values1.= ", `isdeleted`= '0'";
		}
		if($email != '')
		{
			$values1.= ", `email`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values1.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values1.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values1.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values1.= ",`city`= '".$city."'";
		} 

		// update all instance working area details
        if($state != '')
		{
			$values2.= ", `state_ids`= '".$state."'";
		}
		if($city != '')
		{	
			$values2.= ",`city_ids`= '".$city."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values2.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values2.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values2.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values2.= ",`subarea_ids`= ''";
		}


//echo $user_id;
            $new_manufacturer_id = array();
            foreach ($manufacturer_id as $key => $value)
			{
				$new_manufacturer_id[] = $value;
			}
           $COMPID = COMPID;
           array_push($new_manufacturer_id,$COMPID);
           array_push($old_manufacturer_id,$COMPID);

		  $constring1 = array("1"=>array("dbname"=>"salzpoin_znew_uatsaras","dbuser"=>"salzpoin_znew_ua","pwd"=>"b#jc-uoUyyGQ"), "7"=>array("dbname"=>"salzpoin_znew_uatkaysons","dbuser"=>"salzpoin_znew_ua","pwd"=>"b#jc-uoUyyGQ"), "8"=>array("dbname"=>"salzpoin_znew_uatsupplychainz","dbuser"=>"salzpoin_znew_ua","pwd"=>"b#jc-uoUyyGQ"));

		       /*$sql_company ="SELECT companyid from tbl_user_company where userid ='$user_id' GROUP BY companyid";
                $result1 = mysqli_query($this->common_connection,$sql_company);
                $rows = [];
                $old_manufacturer_id = array();
				while($row = mysqli_fetch_assoc($result1)) 
				{
			         $rows[] = $row;
			         $val = $row['companyid'];
			     	 array_push($old_manufacturer_id, $val);
				}*/

				$sql_company12 ="SELECT passwd from tbl_users where uid ='$common_user_id'";
                $result123 = mysqli_query($this->common_connection,$sql_company12);
				while($row23 = mysqli_fetch_assoc($result123)) 
				{
			         $passwd = $row23['passwd'];
				}
				        
				    $manufacturer_id11 = implode(',', $manufacturer_id);
		            $update_manufacturer_sql = "UPDATE tbl_users SET common_user_manufacturers= '$manufacturer_id11' WHERE uid='$common_user_id'";		    
      	             mysqli_query($this->common_connection,$update_manufacturer_sql);
/*echo "<pre>";
print_r($new_manufacturer_id);
echo "<pre>";
print_r($old_manufacturer_id);*/


$all_array_value = array_unique(array_merge($new_manufacturer_id,$old_manufacturer_id));
/*echo "<pre>";
print_r($all_array_value);

exit();*/

   foreach ($all_array_value as $key => $value) 
   { 
			$dbname =  $constring1[$value]['dbname'];
			$dbuser = $constring1[$value]['dbuser'];
			$pwd = $constring1[$value]['pwd'];
			$con5	=	mysqli_connect('localhost', $dbuser, $pwd, $dbname) or die("cannot connect");

	   	$var1=0;$var2=0;
	    if (in_array($value, $new_manufacturer_id)) 
	    {
		   $var1=1;
		}		
		 if (in_array($value, $old_manufacturer_id)) 
	    {
		     $var2=1;
		}

		if ($var2==1 && $var1==1 ) 
	    {
		      	   
		      	      $update_user_sql = "UPDATE tbl_user SET $values1 WHERE common_user_id='$common_user_id'";
		      	      mysqli_query($con5,$update_user_sql);
		}
		else if ($var2==1 && $var1==0 ) 
	    {    	       
		      	       $delete_user_sql = "UPDATE tbl_user SET isdeleted='1' WHERE common_user_id='$common_user_id'";
		      	       mysqli_query($con5,$delete_user_sql);
		}
		else if ($var2==0 && $var1==1 ) 
	    {
		     //insert
	    	/*echo "insert--";
	    	exit();*/	  

            /* $user_sql112 = "SELECT `id` FROM `tbl_user` common_user_id='$common_user_id'";
			 $result172 = mysqli_query($con5,$user_sql112);
              while($row172 = mysqli_fetch_array($result172))
				{					   
						$user_id  =$row172['id'];
				}

             if (isset($user_id)) 
             {
             	  $update_user_sql1 = "UPDATE tbl_user SET isdeleted= '0' WHERE common_user_id='$common_user_id'";		    
      	            mysqli_query($con5,$update_user_sql1);      	           
             }*/
             
            

								 if($user_type == 'Distributor' || $user_type == 'SalesPerson' || $user_type == 'DeliveryPerson')
								{
									$user_sql11 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' AND `is_default_user`='1' LIMIT 1";
									$result17 = mysqli_query($con5,$user_sql11);
									$count = mysqli_num_rows($result17);
									if ($count>0)
									{
									while($row17 = mysqli_fetch_array($result17))
									{
									// $userid1 = $row['id'];
									$external_id_new  =$row17['id'];
									//	echo $external_id_new.'--';
									$sstockist_id  =$row17['id'];
									}
									}
									else
									{
									$user_sql11 = "SELECT `id`,`user_type`,`is_default_user` FROM `tbl_user` WHERE `user_type`='Superstockist' LIMIT 1";
									$result1 = mysqli_query($con5,$user_sql11);
									while($row = mysqli_fetch_array($result1))
									{
									$external_id_new  =$row['id'];
									$sstockist_id  =$row['id'];
									}
									} 
								}
								if($user_type == 'Superstockist' || $user_type == 'DeliveryChannelPerson')
								{
								$external_id_new =1;
								$sstockist_id =0;
								}               

							 $user_sql1 = "INSERT INTO tbl_user (`common_user_id`,`external_id`,`is_common_user`,`firstname`,`username`,`pwd`,`sstockist_id`,`user_type` $fields) 
								VALUES('".$common_user_id."','".$external_id_new."','".$is_common_user."','".$firstname."','".$username."','".$passwd."','".$sstockist_id."','".$user_type."' $values)";
								
								mysqli_query($con5,$user_sql1);

        }


               
					
   } 
  // exit();
	
}











}
?>