<?php
include 'dbmaindb.php';
$con = new Connectionmain();
$cust_data['id'] = $_POST['id'];
$cust_data['customer_name'] = $_POST['customer_name'];
$cust_data['customer_emailid'] = $_POST['customer_emailid'];
$cust_data['customer_phone_no'] = $_POST['customer_phone_no'];
$cust_data['customer_address'] = $_POST['customer_address'];
$cust_data['customer_type'] = $_POST['customer_type'];
$cust_data['customer_shipping_address'] = $_POST['customer_shipping_address'];
$cust_data['state'] = $_POST['state'];
$cust_data['city'] = $_POST['city'];
$cust_data['area'] = $_POST['area'];
$cust_data['subarea'] = $_POST['subarea'];
$cust_data['addr_lat'] = $_POST['addr_lat'];
$cust_data['addr_long'] = $_POST['addr_long'];
$cust_data['ship_addr_lat'] = $_POST['ship_addr_lat'];
$cust_data['ship_addr_long'] = $_POST['ship_addr_long'];
     
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$jsonOutput = $JSONVar->update_customer_data($cust_data);
echo $jsonOutput;
