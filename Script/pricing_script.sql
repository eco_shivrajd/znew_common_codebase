--new changes as dynamic mrp or margin 
ALTER TABLE `tbl_product_variant` ADD `mrp_or_margin_flag` INT NOT NULL COMMENT '1=mrp,2=margin' AFTER `price`;
ALTER TABLE `tbl_product_variant` CHANGE `mrp_or_margin_flag` `mrp_or_margin_flag` INT(11) NOT NULL DEFAULT '2' COMMENT '1=mrp,2=margin';

UPDATE `tbl_product_variant` SET `mrp_or_margin_flag` = '2' ;

--note start
how much user level or user type count that much columns added in tbl_product_variant
mrp or margin
--note end

ALTER TABLE `tbl_product_variant` 
CHANGE `price_ss` `price_level1` FLOAT(8,2) NOT NULL, 
CHANGE `price_dcp` `price_level2` FLOAT(8,2) NOT NULL, 
CHANGE `margin_ss` `price_level3` FLOAT(8,2) NOT NULL, 
CHANGE `margin_dcp` `price_level4` FLOAT(8,2) NOT NULL;

ALTER TABLE `tbl_product_variant` 
ADD `price_level5` FLOAT(8,2) NOT NULL AFTER `price_level4`, 
ADD `price_level6` FLOAT(8,2) NOT NULL AFTER `price_level5`, 
ADD `price_level7` FLOAT(8,2) NOT NULL AFTER `price_level6`, 
ADD `price_level8` FLOAT(8,2) NOT NULL AFTER `price_level7`;

-- add Shopkeeper user type in tbl_user 
ALTER TABLE `tbl_user` CHANGE `user_type` `user_type` 
ENUM('Admin','Superstockist','Distributor','SalesPerson','DeliveryPerson','Accountant',
'DeliveryChannelPerson','Shopkeeper') 
CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
